-- 1. Abfrage mit Selektion und Projektion
-- Geben sie die Telefonnummer(n) aller Personen mit dem Vornamen Dieter aus. 
-- Anmerkung: TelefonNr ist ein Mehrwertiges Attribut
SELECT 
    NAME_VORNAME || ' ' || NAME_NACHNAME AS Name,
    PERSON_TELEFONNUMMER.TELEFONNUMMER AS TelefonNummern
    FROM 
        PERSON INNER JOIN 
            PERSON_TELEFONNUMMER ON 
                PERSON.ID = PERSON_TELEFONNUMMER.PERSON
        WHERE 
            PERSON.NAME_VORNAME = 'Dieter';

-- 2. Abfrage für Join mit 2 Tabellen 
-- Geben sie alle Personen mit den zugehörigen Telefonnummern aus. 
SELECT 
    PERSON.NAME_VORNAME || ' ' || PERSON.NAME_NACHNAME AS Name, 
    PERSON_TELEFONNUMMER.TELEFONNUMMER AS Telefonnummer 
    FROM 
        PERSON INNER JOIN 
            PERSON_TELEFONNUMMER ON 
                PERSON_TELEFONNUMMER.PERSON = PERSON.ID;

-- 3.​Abfrage für Join, Selektion und Projektion mit mindestens 4 Tabellen
-- Geben sie die Namen und Beschreibungen aller Aufgaben aus,die von Mitgliedern übernommen werden,die 2019 eine 
-- Veranstaltung betreut haben. 

SELECT UNIQUE
    A.NAME,A.BESCHREIBUNG
FROM 
    UEBERNIMMTAUFGABE
INNER JOIN
    AUFGABE A ON UEBERNIMMTAUFGABE.AUFGABE = A.ID
INNER JOIN 
    PERSON P ON UEBERNIMMTAUFGABE.PERSON = P.ID
INNER JOIN 
    BETREUTVERANSTALTUNG BV ON BV.PERSON = P.ID
INNER JOIN
    VERANSTALTUNG V ON BV.VERANSTALTUNG = V.ID
WHERE EXTRACT(YEAR FROM V.ZEITPUNKT) = 2019;




-- 4. Abfrage mit sortierter Ausgabe mindestens 2 Sortierkriterien
-- Geben sie die Adressen aller Personen ,aufsteigend nach Postleitzahl und Hausnummer sortiert, aus

SELECT 
    PERSON.NAME_VORNAME || ' ' || PERSON.NAME_NACHNAME AS Name, 
    PERSON.ADRESSE_PLZ, 
    PERSON.ADRESSE_HAUSNUMMER
    FROM
        PERSON
        ORDER BY ADRESSE_PLZ ASC, ADRESSE_HAUSNUMMER ASC

-- 5.Abfrage mit LIKE und einem regulärem Ausdruck
-- Geben sie alle Funktionsträger aus dessen Dienststelle das Wort “Verkehr” beinhaltet.

SELECT 
    PERSON.NAME_VORNAME || ' ' || PERSON.NAME_NACHNAME AS Name,
    PERSON.FUNKTIONSTRAEGER_FUNKTION AS Funktion,
    PERSON.FUNKTIONSTRAEGER_DIENSTSTELLE AS Dienststelle
    FROM
        PERSON
        WHERE 
            (BITAND(TYP,1)+0 = 1) /* ist Funktionsträger */ 
            AND 
            (FUNKTIONSTRAEGER_DIENSTSTELLE LIKE '%Verkehr%');

-- 6.Update-Anweisung mit Unterabfrage und dem Operator “IN”
--  Ändern sie den Namen aller Aufgaben auf “NULL”,bei denen in der Beschreibung das Wort “Test” oder “Beispiel” vorkommt.

UPDATE AUFGABE
    SET NAME = 'NULL' -- null nicht möglich, da notnull constraint gesetzt ist. Deswegen "NULL" als varchar
    (SELECT * FROM AUFGABE WHERE BESCHREIBUNG IN ('Test','Beispiel')) A
    WHERE (ID == A.ID);

SELECT * FROM AUFGABE WHERE NAME = 'NULL';


-- 7.Abfrage mit statistischen Funktionen und GROUP BY
-- Was ist die durchschnittliche Dauer der Radtouren gruppiert nach Schwierigkeit? 

SELECT 
    RADTOUR_SCHWIERIGKEIT AS Schwierigkeit,
    AVG(RADTOUR_DAUER) AS Durchschnittsdauer
    FROM
        VERANSTALTUNG
        WHERE (BITAND(TYP,2)+0 = 2) -- ist Radtour
        GROUP BY RADTOUR_SCHWIERIGKEIT;

-- 8.Abfrage mit LEFT OUTER JOIN
-- Geben sie alle Mitglieder aus,zusammen mit den von ihnen betreuten Veranstaltungen.

SELECT 
    NAME_VORNAME || ' ' || NAME_NACHNAME AS Name,
    V.ID  || ' ' ||  V.NAME AS Veranstaltung
    FROM
        PERSON P
        LEFT OUTER JOIN BETREUTVERANSTALTUNG BV ON P.ID = BV.PERSON
        LEFT OUTER JOIN VERANSTALTUNG V ON V.ID = BV.VERANSTALTUNG
    ORDER BY Name;

            
-- 9.Abfrage, bei der eine Tabelle doppelt vorkommt und daher Aliasnamen verwendet werden müssen
-- Geben sie alle Kurse zusammen mit den Radtouren aus,die am selben Datum stattfinden und nicht beides gleichzeitig sind.

SELECT 
    RADTOUR.ZEITPUNKT,
    RADTOUR.ID,
    RADTOUR.NAME,
    KURS.ID,
    KURS.NAME 
    FROM 
    VERANSTALTUNG  RADTOUR
    JOIN VERANSTALTUNG KURS ON RADTOUR.ZEITPUNKT = KURS.ZEITPUNKT
    WHERE RADTOUR.TYP = 2 AND KURS.TYP = 1

