-- Zu Aufgabe 1
INSERT INTO "PERSON" ("ID","NAME_VORNAME","NAME_NACHNAME","ADRESSE_PLZ","ADRESSE_STADT","ADRESSE_STRASSE","ADRESSE_HAUSNUMMER","EMAIL","TYP","MITGLIED_TYP","MITGLIED_NUMMER","MITGLIED_BEITRITTSDATUM") 
    VALUES (6,'Dieter','Maier','88250','Weingarten','Weingartener Weg',45,'dieter.maier@verein.de',2,'Standartmitglied',2174,TO_DATE('2010-06-02','yyyy-mm-dd'));

    INSERT INTO "PERSON" ("ID","NAME_VORNAME","NAME_NACHNAME","ADRESSE_PLZ","ADRESSE_STADT","ADRESSE_STRASSE","ADRESSE_HAUSNUMMER","EMAIL","TYP","MITGLIED_TYP","MITGLIED_NUMMER","MITGLIED_BEITRITTSDATUM") 
    VALUES (7,'Dieter','Müller','88250','Weingarten','Weinweg',9,'d.mueller@verein.de',2,'Standartmitglied',8295,TO_DATE('2005-01-08','yyyy-mm-dd'));

    INSERT INTO "PERSON_TELEFONNUMMER" ("PERSON","TELEFONNUMMER") 
    VALUES (7,'+49 751229421');

    INSERT INTO "PERSON_TELEFONNUMMER" ("PERSON","TELEFONNUMMER") 
    VALUES (6,'+49 751/321122');

    INSERT INTO "PERSON_TELEFONNUMMER" ("PERSON","TELEFONNUMMER") 
    VALUES (6,'+49 751/3218722');

    INSERT INTO "PERSON_TELEFONNUMMER" ("PERSON","TELEFONNUMMER") 
    VALUES (7,'+49 751/9218722');

-- Aufgabe 2

-- keine neuen Daten benötigt

-- Aufgabe 3

-- TODO

-- Aufgabe 4

-- keine neuen Daten benötigt

-- Aufgabe 5

INSERT INTO "PERSON" ("ID","NAME_VORNAME","NAME_NACHNAME","ADRESSE_PLZ","ADRESSE_STADT","ADRESSE_STRASSE","ADRESSE_HAUSNUMMER","EMAIL","TYP","FUNKTIONSTRAEGER_FUNKTION","FUNKTIONSTRAEGER_DIENSTSTELLE")
VALUES (8, 'Annika','Gries','12345','Musterstadt','Musterstaße',91,'a.griess@musterstadt.de',1,'Verkehrsminister','Verkehrsminister Musterstadt') ;

INSERT INTO "PERSON" ("ID","NAME_VORNAME","NAME_NACHNAME","ADRESSE_PLZ","ADRESSE_STADT","ADRESSE_STRASSE","ADRESSE_HAUSNUMMER","EMAIL","TYP","FUNKTIONSTRAEGER_FUNKTION","FUNKTIONSTRAEGER_DIENSTSTELLE")
VALUES (9, 'Alina','Weber','12345','Musterstadt','Musteralee',101,'a.weber@musterstadt.de',1,'Verkehrsbeauftragte','Verkehrsbeauftragte Musterstadt') ;

INSERT INTO "PERSON_TELEFONNUMMER" ("PERSON","TELEFONNUMMER") 
VALUES (8,'+49 788/456654');

INSERT INTO "PERSON_TELEFONNUMMER" ("PERSON","TELEFONNUMMER") 
VALUES (9,'+49 788/459744');

-- Aufgabe 6

INSERT INTO "AUFGABE" 
VALUES ('Beispielaufgabe 1','Test kommt vor',5);

INSERT INTO "AUFGABE" 
VALUES ('Beispielaufgabe 2','Beispiel kommt vor',6);

INSERT INTO "AUFGABE" 
VALUES ('Beispielaufgabe 3','T_st oder B_ispiel kommt nicht vor',7);

-- Aufgabe 7

-- keine neuen Daten benötigt

-- Aufgabe 8

-- keine neuen Daten benötigt

-- Aufgabe 9

-- keine neuen Daten benötigt


