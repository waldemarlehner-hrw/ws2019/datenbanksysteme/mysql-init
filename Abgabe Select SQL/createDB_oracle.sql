



 CREATE TABLE "AUFGABE"  
   (	
	"NAME" VARCHAR2(50) NOT NULL,  
	"BESCHREIBUNG" VARCHAR2(200), 
	"ID" NUMBER(*,0)  NOT NULL ,  
	 CONSTRAINT "AUFGABE_PK" PRIMARY KEY ("ID")  -- Die Spalte ID wird als Primary Key gesetzt
   )
 
;

  CREATE TABLE "PERSON"  
   (
	"ID" NUMBER(*,0)  NOT NULL,  
	"NAME_VORNAME" VARCHAR2(50) NOT NULL,  
	"NAME_NACHNAME" VARCHAR2(50) NOT NULL,  
	"ADRESSE_PLZ" CHAR(5),  
	"ADRESSE_STADT" VARCHAR2(50),  
	"ADRESSE_HAUSNUMMER" NUMBER(*,0),  
	"ADRESSE_STRASSE" VARCHAR2(50),  
	"EMAIL" VARCHAR2(100),  
	"TYP" NUMBER(*,0) NOT NULL CONSTRAINT person_typ CHECK("TYP" BETWEEN 1 AND 3),  -- siehe COMMENT ON Befehl
	"MITGLIED_TYP" VARCHAR2(50),  
	"MITGLIED_NUMMER" NUMBER(*,0) UNIQUE,  
	"MITGLIED_BEITRITTSDATUM" DATE,  
	"FUNKTIONSTRAEGER_FUNKTION" VARCHAR2(50),  
	"FUNKTIONSTRAEGER_DIENSTSTELLE" VARCHAR2(50),  
	 CONSTRAINT "PERSON_PK" PRIMARY KEY ("ID") -- Die Spalte ID wird als Primary Key gesetzt
   ) 
;
	COMMENT ON COLUMN "PERSON"."TYP" IS '0b01 -> Funktionstraeger, 0b10 -> Mitglied, 0b11 -> Funktionstraeger und Mitglied';


  CREATE TABLE "VERANSTALTUNG"  
   (	
    "ID" NUMBER(*,0)  NOT NULL,  
	"NAME" VARCHAR2(50) NOT NULL,  
	"BESCHREIBUNG" VARCHAR2(100),  
	"TREFFPUNKT_BEZEICHNUNG" VARCHAR2(100) NOT NULL, -- Zusammengesetztes Attribut "Treffpunkt" 
	"TREFFPUNKT_BREITENGRADE" FLOAT(126) NOT NULL,  
	"TREFFPUNKT_LAENGENGRADE" FLOAT(126) NOT NULL,  
	"TYP" NUMBER(*,0) NOT NULL CONSTRAINT veranstaltung_typ CHECK("TYP" BETWEEN 0 AND 3),  -- siehe COMMENT ON Befehl
	"KURS_TEILNAMEGEBUEHREN" NUMBER(*,0) DEFAULT 0 CONSTRAINT pos_tnmgb CHECK ("KURS_TEILNAMEGEBUEHREN" >= 0),  -- Nur für Kurse, KURSE_LERNZIELE sind in einer separaten Tabelle aufzufinden. Der Constraint besagt dass der Wert nicht kleiner 0 sein darf
	"RADTOUR_DAUER" NUMBER(*,0) CONSTRAINT pos_rdt_dauer CHECK ("RADTOUR_DAUER" > 0), -- Constraint besagt, dass die Dauer > 0 sein muss 
	"RADTOUR_DISTANZ" NUMBER(*,0) CONSTRAINT pos_rdt_dist CHECK ("RADTOUR_DISTANZ" > 0),  -- Constraint besagt, dass die DIstanz > 0 sein muss
	"RADTOUR_RADEMPFEHLUNG" VARCHAR2(50),  
	"RADTOUR_SCHWIERIGKEIT" VARCHAR2(50),  
	"ZEITPUNKT" DATE,  
	 CONSTRAINT "VERANSTALTUNG_PK" PRIMARY KEY ("ID") -- Die Spalte ID wird als Primary Key gesetzt
   ) 
;

   COMMENT ON COLUMN "VERANSTALTUNG"."TYP" IS '0b00 -> Generische Veranstaltung, 0b01 -> Kurs 0b10 -> Radtour, 0b11 -> Kurs und Radtour';



   COMMENT ON COLUMN "VERANSTALTUNG"."RADTOUR_DAUER" IS 'in Stunden';



   COMMENT ON COLUMN "VERANSTALTUNG"."RADTOUR_DISTANZ" IS 'in km';



  CREATE TABLE "BETREUTVERANSTALTUNG"  -- Es handelt sich um eine MxN Relation. Deshalb ist diese Übergangstabelle notwendig
   (	
	"PERSON" NUMBER(*,0) NOT NULL,  
	"VERANSTALTUNG" NUMBER(*,0) NOT NULL,  
	 CONSTRAINT "FK_PERSON" FOREIGN KEY ("PERSON") -- Im Feld Person wird die Referenz auf die Person gespeichert
	  REFERENCES "PERSON" ("ID"),  
	 CONSTRAINT "FK_VERANSTALTUNG" FOREIGN KEY ("VERANSTALTUNG") -- Im Feld Veranstaltung wird die Referenz auf die Veranstaltung gespeichert
	  REFERENCES "VERANSTALTUNG" ("ID") 
   ) 
;

   COMMENT ON COLUMN "BETREUTVERANSTALTUNG"."PERSON" IS 'Die Person, welche die Veranstaltung betreut';



   COMMENT ON COLUMN "BETREUTVERANSTALTUNG"."VERANSTALTUNG" IS 'Die Veranstaltung, welche betreut wird';



  CREATE TABLE "PERSON_TELEFONNUMMER"  -- Telefonnummer ist als mehrwertiges Atribut definiert. Deshalb werden diese in einer separaten Tabelle abgespeichert
   (	
	"PERSON" NUMBER(*,0) NOT NULL,  
	"TELEFONNUMMER" VARCHAR2(50) NOT NULL,  
	 CONSTRAINT "FK_PERSON_TELEFONNUMMER" FOREIGN KEY ("PERSON") -- Referenz auf die Person
	  REFERENCES "PERSON" ("ID") 
   ) 
;

  CREATE TABLE "UEBERNIMMTAUFGABE"  -- MxN Relation, weshalb diese Übergangstabelle benötigt wird
   (	
	"PERSON" NUMBER(*,0) NOT NULL,  
	"AUFGABE" NUMBER(*,0) NOT NULL,  
	"ANFANGSDATUM" DATE NOT NULL,  
	"ENDDATUM" DATE,  
	 CONSTRAINT "FK_AUFGABE" FOREIGN KEY ("AUFGABE") -- Referenz auf die Aufgabe
	  REFERENCES "AUFGABE" ("ID"),  
	 CONSTRAINT "FK_PERSON_AUFGABE" FOREIGN KEY ("PERSON") -- Referenz auf die Person
	  REFERENCES "PERSON" ("ID") 
   ) 
;

   COMMENT ON COLUMN "UEBERNIMMTAUFGABE"."ENDDATUM" IS 'wenn null: noch aktiv taetig';



  CREATE TABLE "VERANSTALTUNG_LERNZIELE"  -- Lernziele ist als mehrwertiges Atribut definiert. Deshalb werden diese in einer separaten Tabelle abgespeichert
   (	
	"VERANSTALTUNG" NUMBER(*,0) NOT NULL,  
	"LERNZIEL" VARCHAR2(100) NOT NULL,  
	 CONSTRAINT "FK_VERANSTALTUNG_LERNZIELE" FOREIGN KEY ("VERANSTALTUNG") -- Referenz auf die Veranstaltung
	  REFERENCES "VERANSTALTUNG" ("ID") 
   ) 
;
