/* Lösche die alte Datenbank, wenn vorhanden */

DROP DATABASE IF EXISTS `verein`;
CREATE DATABASE `verein`;



-- ########## ES BEGINNT AUTOMATISCH GENERIERTER CODE VON PHPMYADMIN ##########
-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 12. Nov 2019 um 17:58
-- Server-Version: 10.4.8-MariaDB
-- PHP-Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `verein`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aufgabe`
--

CREATE TABLE `aufgabe` (
  `beschreibung` varchar(200) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Eine Aufgabe';

--
-- Daten für Tabelle `aufgabe`
--

INSERT INTO `aufgabe` (`beschreibung`, `name`, `id`) VALUES
('Hält das Vereinskonto, überprüft Einnahmen und Ausgaben', 'Kontomanager', 1),
('Ansprechperson für Presse und Co.', 'Ansprechperson', 2),
('Überprüft Finanzen, schaut dass Ausgaben des Vereins nicht die Einahmen übertreffen', 'Finanzmanager', 3),
('Organisiert Vereinsveranstaltungen und verteilt Betreuer', 'Eventorganisation', 4);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `betreutveranstaltung`
--

CREATE TABLE `betreutveranstaltung` (
  `in` int(10) UNSIGNED NOT NULL COMMENT 'Person welche betreut',
  `out` int(10) UNSIGNED NOT NULL COMMENT 'Veranstaltung welche betreut wird'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `person`
--

CREATE TABLE `person` (
  `id` int(10) UNSIGNED NOT NULL,
  `vorname` varchar(50) NOT NULL,
  `nachname` varchar(50) NOT NULL,
  `adresse.plz` char(5) DEFAULT NULL,
  `adresse.stadt` varchar(50) DEFAULT NULL,
  `adresse.strasse` varchar(50) DEFAULT NULL,
  `adresse.hausnummer` mediumint(8) UNSIGNED DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `typ` bit(2) NOT NULL COMMENT 'Erstes Bit sagt aus ob Mitglied, Zweites ob Funktionsträger, wert darf nicht 0b00 sein',
  `mitglied.typ` varchar(50) DEFAULT NULL,
  `mitglied.nummer` int(10) UNSIGNED DEFAULT NULL,
  `mitglied.beitrittsdatum` date DEFAULT NULL,
  `funktionstraeger.funktion` varchar(50) DEFAULT NULL,
  `funktionstraeger.dienststelle` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Eine Person, kann ein Mitglied, ein Funktionsträger, oder beides sein';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `person.telefonnummern`
--

CREATE TABLE `person.telefonnummern` (
  `personid` int(10) UNSIGNED NOT NULL,
  `telefonnummer` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `uebernimmtaufgabe`
--

CREATE TABLE `uebernimmtaufgabe` (
  `in` int(10) UNSIGNED NOT NULL COMMENT 'von wem die Aufgabe uebernommen wird',
  `out` int(10) UNSIGNED NOT NULL COMMENT 'welche Aufgabe uebernommen wurde',
  `anfangsdatum` date NOT NULL COMMENT 'wann die Aufgabe aufgenommen wurde',
  `enddatum` date DEFAULT NULL COMMENT 'wann die Aufgabe aufgehört wurde. Wenn null, wird diese noch aktiv uebernommen'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `veranstaltung`
--

CREATE TABLE `veranstaltung` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `beschreibung` varchar(200) DEFAULT NULL,
  `treffpunkt.bezeichnung` varchar(100) DEFAULT NULL,
  `treffpunkt.breitengrade` double NOT NULL,
  `treffpunkt.laengengrade` double NOT NULL,
  `zeitpunkt` datetime NOT NULL,
  `typ` bit(2) NOT NULL COMMENT 'erstes Bit = ist Radtour, zweites bit = ist Kurs',
  `kurs.teilnamegebuehren` decimal(6,2) NOT NULL COMMENT 'xxxx.xx',
  `radtour.dauer` int(11) DEFAULT NULL COMMENT 'in minuten',
  `radtour.distanz` float DEFAULT NULL COMMENT 'in km',
  `radtour.radempfehlung` varchar(100) DEFAULT NULL,
  `radtour.schwierigkeit` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `veranstaltung.lernziele`
--

CREATE TABLE `veranstaltung.lernziele` (
  `veranstaltungid` int(10) UNSIGNED NOT NULL,
  `lernziel` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `aufgabe`
--
ALTER TABLE `aufgabe`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `betreutveranstaltung`
--
ALTER TABLE `betreutveranstaltung`
  ADD KEY `betreutveranstaltung_fk` (`in`),
  ADD KEY `betreutveranstaltung_fk_1` (`out`);

--
-- Indizes für die Tabelle `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `person.telefonnummern`
--
ALTER TABLE `person.telefonnummern`
  ADD KEY `person_telefonnummern_fk` (`personid`);

--
-- Indizes für die Tabelle `uebernimmtaufgabe`
--
ALTER TABLE `uebernimmtaufgabe`
  ADD KEY `uebernimmtaufgabe_fk` (`in`),
  ADD KEY `uebernimmtaufgabe_fk_1` (`out`);

--
-- Indizes für die Tabelle `veranstaltung`
--
ALTER TABLE `veranstaltung`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `veranstaltung.lernziele`
--
ALTER TABLE `veranstaltung.lernziele`
  ADD KEY `veranstaltung_lernziele_fk` (`veranstaltungid`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `aufgabe`
--
ALTER TABLE `aufgabe`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `person`
--
ALTER TABLE `person`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `veranstaltung`
--
ALTER TABLE `veranstaltung`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `betreutveranstaltung`
--
ALTER TABLE `betreutveranstaltung`
  ADD CONSTRAINT `betreutveranstaltung_fk` FOREIGN KEY (`in`) REFERENCES `person` (`id`),
  ADD CONSTRAINT `betreutveranstaltung_fk_1` FOREIGN KEY (`out`) REFERENCES `veranstaltung` (`id`);

--
-- Constraints der Tabelle `person.telefonnummern`
--
ALTER TABLE `person.telefonnummern`
  ADD CONSTRAINT `person_telefonnummern_fk` FOREIGN KEY (`personid`) REFERENCES `person` (`id`);

--
-- Constraints der Tabelle `uebernimmtaufgabe`
--
ALTER TABLE `uebernimmtaufgabe`
  ADD CONSTRAINT `uebernimmtaufgabe_fk` FOREIGN KEY (`in`) REFERENCES `person` (`id`),
  ADD CONSTRAINT `uebernimmtaufgabe_fk_1` FOREIGN KEY (`out`) REFERENCES `aufgabe` (`id`);

--
-- Constraints der Tabelle `veranstaltung.lernziele`
--
ALTER TABLE `veranstaltung.lernziele`
  ADD CONSTRAINT `veranstaltung_lernziele_fk` FOREIGN KEY (`veranstaltungid`) REFERENCES `veranstaltung` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


-- ########## ENDE VON AUTOMATISCH GENERIERTEM CODE VON SQLITE EXPORT ##########
